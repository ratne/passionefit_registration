const functions = require('firebase-functions');
const axios = require('axios');
const cors = require('cors')({origin: true});
const stripe = require('stripe')('sk_live_XWl0ZQFqQsxTbVsQivLn6rIy00wxmlzpdz');


exports.paystripe = functions.https.onRequest((req, res) => {
  axios.get('https://passionefitsculptureburning.firebaseio.com/price.json').then(
    (response) => {
      console.log(response);
      let paymentDetails = response.data;
      cors(req, res, () => {
        const thisReqMethod = req.method;
        // let amount = paymentDetails.amount;
        let currency = paymentDetails.currency;
        let description = paymentDetails.description;
        let thisIsTheMessage = 'Make a payment with Stripe!';

        if (thisReqMethod === 'POST') {
          let token = req.body.token;
          let amount = req.body.amount;
          stripe.charges.create({
            amount: amount,
            currency: currency,
            description: description,
            source: token,
          }).then((result) => {
            console.log(result);
            res.status(200)
              .send('success');
            return res
          }).catch((error) => {
            console.log(error);
            res.status(200).send('error');
            return res;
          });
        } else {
          return res.status(200).send(thisIsTheMessage);
        }
        return 0
      });
      return 0
    }).catch((error) => {
    res.status(200).send('error! not getting payment from Database');
    console.log(error);
  });


});
