import Registration from "./components/Registration";
import SendResetEmail from "./components/SendResetEmail";

export const routes = [
  {path: '/register', component: Registration, name: 'register'},
  {path:'/resetPassword', component: SendResetEmail, name: 'sendResetCode'},
];
