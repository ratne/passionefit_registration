import Vue from 'vue'
import App from './App.vue'
import {Vuelidate} from "vuelidate";
import Vuetify from 'vuetify'
import VueRouter from 'vue-router';
import 'vuetify/dist/vuetify.min.css'
import axios from "axios";
import store from './store'
import {routes} from "./router";



Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.use(Vuetify, {
  theme: {
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
});

export const dataBus = new Vue(
  {
    data() {
      return {
      updatedValues: [],
        isPaid:false,
      }
    },
    beforeCreate() {
      axios.get('https://passionefitsculptureburning.firebaseio.com/price.json').then(
        (response) => {
          return response.data;
        }).then(
        (data) => {
          dataBus.$emit('updatedData', data);
        });
    },
    });

const router = new VueRouter({
  routes: routes,
  mode: 'history'
});

new Vue({
  store,
  router: router,
  render: h => h(App)
}).$mount('#app');
